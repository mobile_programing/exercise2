import 'dart:io';

bool PrimeNum(num) {
  //รับค่า num จากผู้ใช้
  for (int i = 2; i <= num; i++) {
    //loop check
    if (num % 2 == 0) {
      return false;
    }
  }
  return true;
}

void main(List<String> args) {
  var num = int.parse(stdin.readLineSync()!);
  if (PrimeNum(num)) {
    print("$num is prime number"); //จน.เฉพาะ
  } else {
    print("$num is not prime number"); //ไม่ใช่
  }
}
